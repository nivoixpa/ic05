
# IC05

## Description of the project
This repository contains files for the end of semester project in the IC05 class : Critical analysis of digital data.

The goal of the project is to design and implement two kinds of recommendation algorithms for Steam users. These algorithms should use accessible data to construct databases and linking tables between games to recommend a game to a user based on their current Steam library.

# Cloning the project
This repository contains python scripts and data files used to implement content based recommendation algorithms.

You can clone this repo with HTTP using the command : ```git clone https://gitlab.utc.fr/nivoixpa/ic05.git```.

# How to use

This repository contains two main scripts.

**Generate-nodes-edges.py** : This script is used to fetch the Steam API database and generate two csv files which should then be imported to the gephi project for processing, using the Multimode Networks Transformation plugin. To launch the script, you can use the following command : 
>```python3 .\generate-nodes-edges.py```.

\
**Get-recommendation.py** : This script allows you to get a list of 10 recommended games based on a game or a Steam user's  games library. To get a recommendation from your Steam games library, you will need an API key which you can find [here](https://steamcommunity.com/dev/apikey), paired with your Steam User ID. You can launch the scripts with the following commands :
>```python3 .\get-recommendation.py -g 'Name of the game'```\
>```python3 .\get-recommendation.py -l 'API key' 'User ID'```

## How it works

For this project, we are using two different kinds of algorithms : content-based filtering and collaborative filtering. This repository only contains content based filtering.


# Content based filtering

This method of recommendation algorithms is based on a linking table that is made by computing the *distance* between games. We construct a giant matrix that links the games based on their similarity using several parameters. In our project, the parameters of similarity are : *developers*, *publishers*, *required_age*, *controller_support* and *tags*.


# Collaborative filtering

This method of recommendation algorithms is also based on a linking table that is made by computing the *distance* between games. However, the difference between this method and content-based filtering lies in the definition of the distance. With collaborative filtering, two games are considered 'linked' when many players play them both for a good amount of time. This kind of algorithms is best when the amount of players in your database is high.
