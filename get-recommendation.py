import csv
import numpy as np
import requests
import os
import sys

# This function creates two dictionaries linking games names to their index in the games list
def CreateGameToIndexDict(path):
	gameToIndex = {}
	indexToGame = {}
	with open(path, 'r') as csvFile:
		reader = csv.DictReader(csvFile)
		last = ''
		index = 0
		for row in reader:
			if last != row['Label']:
				last = row['Label']
				gameToIndex[last.strip()] = index
				indexToGame[index] = last.strip()
				index += 1
	return (gameToIndex, indexToGame)

# This function creates a numpy array containing the similarity between all the games
def PopulateGamesSimilarityArray(path, gameToIndex):
	similarity = np.zeros((len(gameToIndex), len(gameToIndex)))
	with open(path, 'r') as csvFile:
		reader = csv.DictReader(csvFile)
		for row in reader:
			similarity[gameToIndex[row['Source']], gameToIndex[row['Target']]] = row['Weight']
			similarity[gameToIndex[row['Target']], gameToIndex[row['Source']]] = row['Weight']
	return similarity

# This function returns a list of games that are the most similar to the given game
def GetRecommendation(game, similarity, gameToIndex, num):
	gameIndex = gameToIndex.get(game, -1)
	if gameIndex == -1:
		print("Could not find game in database")
		return
	
	indices = np.argpartition(similarity[gameToIndex[game]], -num)[-num:]
	indices = indices[np.argsort(similarity[gameToIndex[game]][indices])][::-1]
	return indices

def SaveLibraryToFile(path, apiKey, playerSteamId):
	response = requests.get("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" + apiKey + "&steamid=" + playerSteamId + "&format=json")
	games = response.json()['response']['games']
	with open(path, 'w', newline='') as csvFile:
		writer = csv.writer(csvFile)
		writer.writerow(['Game', 'Playtime'])
		for game in games:
			response2 = requests.get("http://store.steampowered.com/api/appdetails?appids=" + str(game['appid']))
			try:
				name = response2.json()[str(game['appid'])]['data']['name']
				print(name)
				row = [name.encode('ascii', 'ignore').decode(), game['playtime_forever']]
				writer.writerow(row)
			except UnicodeEncodeError:
				print('Unicode encode error with game :', game['appid'], response2.json()[str(game['appid'])]['data']['name'])
				continue
			except:
				print('Error with game :', game['appid'])
				continue

def GetRecommendationFromLibrary(library, similarity, gameToIndex, num, weights=None):
	indices = np.zeros(num)
	if weights is None:
		weights = np.ones(len(library))
	scores = np.zeros(len(gameToIndex))
	for i in range(len(library)):	
		if library[i] in gameToIndex:
			for game in gameToIndex.keys():
				try:
					scores[gameToIndex[game]] += similarity[gameToIndex[library[i]], gameToIndex[game]] * weights[i]
				except:
					break
		else:
			print("Could not find game in database :", library[i])
	indices = np.argpartition(scores, -num)[-num:]
	indices = indices[np.argsort(scores[indices])][::-1]
	return indices, scores

def GetRecommendationFromLibraryFile(path, similarity, gameToIndex, num):
	with (open(path, 'r')) as csvFile:
		reader = csv.DictReader(csvFile)
		library = []
		weights = []
		w = 0
		for row in reader:
			library.append(row['Game'])
			weights.append(int(row['Playtime']))
			w += int(row['Playtime'])
		return GetRecommendationFromLibrary(library, similarity, gameToIndex, num, [weight / w for weight in weights])

if __name__ == '__main__':
	args = sys.argv[1:]
	gameToIndex, indexToGame = CreateGameToIndexDict('nodes.csv')
	similarity = PopulateGamesSimilarityArray('similarity.csv', gameToIndex)
	if len(args) == 2 and args[0] == '-g' or args[0] == '-game':
		game = args[1]
		if game not in gameToIndex:
			print("Could not find game in database")
			exit()
		recos = GetRecommendation(game, similarity, gameToIndex, 10)
		print("Recommendations for [", game, "]")
		for g in recos:
			print("\t[", indexToGame[g], "] - Score :", similarity[g, gameToIndex[game]])
	
	elif len(args) == 3 and args[0] == '-l' or args[0] == '-library':
		apiKey = args[1]
		playerSteamId = args[2]
		print('Fetching library...')
		SaveLibraryToFile('library.csv', apiKey, playerSteamId)
		recos, scores = GetRecommendationFromLibraryFile('library.csv', similarity, gameToIndex, 10)
		print("Recommendations for library -", playerSteamId)
		for g in recos:
			print("\t[", indexToGame[g], "] - Score :", scores[g])
		os.remove('library.csv')
