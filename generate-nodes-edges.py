import requests
import csv
import json
from lxml import etree
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import os

# This function returns a list of all the games ids on steam
def GetIdList(url):
	browser = webdriver.Firefox()
	browser.get(url)

	select = Select(browser.find_element(By.NAME, 'table-apps_length'))
	select.select_by_value("1000")

	html = browser.page_source
	tree = etree.HTML(html)
	ids = tree.xpath('//tr[contains(@class, "app")]/@data-appid')
	browser.close()
	return ids

# This function makes a call to the steam api to get the game entry (name, tags, etc...)
def GetGameEntryFromID(id):
	print("Treating game : ", id, end=" - ")
	response = requests.get("http://store.steampowered.com/api/appdetails?appids=" + str(id))
	try:
		if not response.json()[str(id)]['success']:
			print("Error")
			return
		game = response.json()[str(id)]['data']
	except:
		print("Exception")
		return

	entry = {}
	specs = ['name', 'developers', 'publishers', 'required_age', 'controller_support']
	for spec in specs:
		if spec in game:
			entry[spec] = game[spec]
		else:
			entry[spec] = 'NA'
	print(entry['name'])

	tags = []
	response = requests.get("http://store.steampowered.com/app/" + str(id))
	tree = etree.HTML(response.text)
	val = tree.xpath('//a[@class="app_tag"]/text()')
	for tag in val:
		tags.append(tag.replace('\t', "").replace('\r', "").replace('\n', ""))
	entry['tags'] = tags
	
	return entry

# This function returns a list of all the games entries from ids list by making steam api calls
def GetGamesFromIdList(ids):
	games = []
	i = 0
	for id in ids:
		print("[", i, "]", end=" ")
		i += 1
		game = GetGameEntryFromID(id)
		if game is not None:
			games.append(game)
	return games

def SaveGameEntries(filePath, games):
	jsonString = json.dumps(games, indent=4)
	with open(filePath, 'w') as f:
		f.write(jsonString)

# This function generates a json file containing all the games entries
def GenerateGamesJson():
	url = "https://steamdb.info/graph/"
	ids = GetIdList(url)
	games = GetGamesFromIdList(ids)
	SaveGameEntries('steam_games.json', games)

# This function generates two csv files : 
# 	a nodes.csv file that contains all the games names and all the tags names 
# 	an edges.csv file that contains all the links between games and tags
def GenerateNodesAndEdgesCSV():
	with open('steam_games.json', 'r') as jsonFile:
		data = json.load(jsonFile)
		games = []
		others = []
		with open('edges.csv', 'w', newline='') as csvFile:
			writer = csv.writer(csvFile)
			writer.writerow(['Source', 'Target', 'Weight'])
			for game in data:
				name = game['name'].encode('ascii', 'ignore').decode().strip()
				if name not in games:
					games.append(name)
				weight = len(game['tags'])
				for tag in game['tags']:
					tag = tag.encode('ascii', 'ignore').decode().strip()
					if tag not in others:
						others.append(tag)
					row = [name, tag, (weight / len(game['tags'])) * 4 + 1]
					writer.writerow(row)
					weight -= 1
		with open('nodes.csv', 'w', newline='') as csvFile:
			writer = csv.writer(csvFile)
			writer.writerow(['ID', 'Label', 'Type'])
			for game in games:
				writer.writerow([game, game, 'Game'])
			for other in others:
				writer.writerow([other, other, 'Tag'])

if __name__ == '__main__':
	print("Generating games json file...")
	GenerateGamesJson()
	print("Generating nodes and edges csv files...")
	GenerateNodesAndEdgesCSV()
	os.remove('steam_games.json')
	os.remove('geckodriver.log')
	